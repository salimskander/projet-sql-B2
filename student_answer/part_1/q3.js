const q3 = `SELECT move.name AS 'Nom', move.category AS 'Catégorie', move.power AS 'Puissance', move.pp AS 'Point de pouvoir', move.accuracy AS 'Précision', move.description AS 'Description'
FROM move 
JOIN type ON type.type_id = move.type_id
WHERE type.name = 'Roche'
ORDER BY category`;

module.exports = q3;