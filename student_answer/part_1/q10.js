const q10 = `SELECT
type.name AS 'Type',
(SELECT move.name
 FROM move
 WHERE type.type_id = move.type_id
 ORDER BY move.power DESC
 LIMIT 1) AS 'Meilleure capacité',
(SELECT MAX(move.power)
 FROM move
 WHERE type.type_id = move.type_id) AS 'Meilleure puissance',
(SELECT move.name
 FROM move
 WHERE type.type_id = move.type_id
 ORDER BY move.power
 LIMIT 1) AS 'Pire capacité',
(SELECT MIN(move.power)
 FROM move
 WHERE type.type_id = move.type_id) AS 'Pire puissance'
FROM
type`;

module.exports = q10;