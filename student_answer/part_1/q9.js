
const q9 = `SELECT
    type.name AS 'Type',
    move.name AS 'Capacité',
    move.power AS 'Puissance',
    move.accuracy AS 'Précision',
    move.description AS 'Description'

FROM
    type
JOIN
    move ON type.type_id = move.type_id

WHERE
    move.power > 100
    AND move.accuracy >= 90
    AND LOWER(move.description) NOT LIKE '%tour%'

ORDER BY
    move.power DESC`;

    module.exports = q9;