const q12 = `SELECT
pokemon.pokedex_number AS 'N°',
pokemon.name AS 'Nom du pokemon',
ability.name AS 'Nom du talent'
FROM
pokemon
JOIN
pokemon_ability ON pokemon.pokemon_id = pokemon_ability.pokemon_id
JOIN
ability ON pokemon_ability.ability_id = ability.ability_id
JOIN
type ON pokemon.type1_id = type.type_id OR pokemon.type2_id = type.type_id
WHERE
type.name = 'Acier'
AND ability.isHidden = 1
ORDER BY
pokemon.pokedex_number`;

module.exports = q12;