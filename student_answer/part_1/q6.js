const q6 = `SELECT
    pokemon.pokedex_number AS "N°",
    pokemon.name AS "Pokemon",
    ability.name AS "Talent",
    ability.description AS "Description du talent"
FROM
    pokemon
JOIN
    pokemon_ability ON pokemon.pokemon_id = pokemon_ability.pokemon_id
JOIN
    ability ON ability.ability_id = pokemon_ability.ability_id
WHERE
    pokemon.pokemon_id IN (
        SELECT DISTINCT pa1.pokemon_id
        FROM pokemon_ability pa1
        LEFT JOIN pokemon_ability pa2 ON pa1.ability_id = pa2.ability_id AND pa1.pokemon_id != pa2.pokemon_id
        WHERE pa2.ability_id IS NULL
    )
ORDER BY
    pokemon.pokedex_number ASC`;

    module.exports = q6;


    
    
    