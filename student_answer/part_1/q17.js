const q17 = `SELECT
    t.name AS 'Type',
    COUNT(m.move_id) AS 'Nombre capacités',
    ROUND(100.0 * SUM(CASE WHEN t.type_id = m.type_id AND m.accuracy = 100 THEN 1 ELSE 0 END) / COUNT(m.move_id), 2) || '%' AS 'Capacité précision 100%',
    ROUND(100.0 * SUM(CASE WHEN t.type_id = m.type_id AND m.category = 'Physique' THEN 1 ELSE 0 END) / COUNT(m.move_id), 2) || '%' AS 'Pourcentage capacité physique',
    ROUND(100.0 * SUM(CASE WHEN t.type_id = m.type_id AND m.category = 'Spéciale' THEN 1 ELSE 0 END) / COUNT(m.move_id), 2) || '%' AS 'Pourcentage capacité spéciale',
    ROUND(100.0 * SUM(CASE WHEN t.type_id = m.type_id AND m.category = 'Statut' THEN 1 ELSE 0 END) / COUNT(m.move_id), 2) || '%' AS 'Pourcentage capacité statut',
    ROUND(100.0 * SUM(CASE WHEN t.type_id = m.type_id AND m.power IS NOT NULL AND m.power <= 40 THEN 1 ELSE 0 END) / COUNT(m.move_id), 2) || '%' AS 'Pourcentage capacité faible',
    ROUND(100.0 * SUM(CASE WHEN t.type_id = m.type_id AND m.power IS NOT NULL AND m.power >= 100 THEN 1 ELSE 0 END) / COUNT(m.move_id), 2) || '%' AS 'Pourcentage capacité forte'
FROM
    type t
LEFT JOIN
    move m ON 1 = 1
GROUP BY
    t.type_id
`;

module.exports = q17;
