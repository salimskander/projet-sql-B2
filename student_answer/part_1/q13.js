const q13 = `SELECT
p.name AS 'Nom du pokemon',
COUNT(DISTINCT m.move_id) AS 'Nombre de capacité avec le meme type'
FROM
pokemon p
JOIN
move m ON p.type1_id = m.type_id OR p.type2_id = m.type_id
GROUP BY
p.pokemon_id
ORDER BY
'Nombre de capacité avec le meme type' DESC, 'Nom du pokemon' ASC
LIMIT
10`;

module.exports = q13;