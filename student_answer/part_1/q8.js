const q8 = `SELECT
    t.name AS 'Nom du type',
    COUNT(DISTINCT p.pokemon_id) AS 'Nombre de pokemon',
    COUNT(DISTINCT CASE WHEN pt.slot = 1 THEN p.pokemon_id END) AS 'Nombre de pokemon avec le type slot 1',
    COUNT(DISTINCT CASE WHEN pt.slot = 2 THEN p.pokemon_id END) AS 'Nombre de pokemon avec le type slot 2'
FROM
    type t
JOIN
    pokemon_type pt ON t.type_id = pt.type_id
JOIN
    pokemon p ON pt.pokemon_id = p.pokemon_id
GROUP BY
    t.type_id
ORDER BY
    COUNT(DISTINCT p.pokemon_id) DESC, COUNT(DISTINCT CASE WHEN pt.slot = 1 THEN p.pokemon_id END) DESC`;

    module.exports = q8;
